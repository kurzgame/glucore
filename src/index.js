var GL = require("GL");

module.exports = {
    "utils": GL.utils,

    "VarInfo": GL.VarInfo,
    "Vertex": GL.Vertex,
    "Fragment": GL.Fragment,

    "Program": GL.Program,

    "Texture": GL.Texture,
    "ArrayBuffer": GL.ArrayBuffer,
    "FrameBuffer": GL.FrameBuffer,

    "TextureMatricesWrapper": require("TextureMatricesWrapper")
};
