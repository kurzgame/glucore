# glucore (0.0.9 alpha)

Set of helper classes for webgl 2d processing.

## Reason

The main purpose is - learning of how webgl works from the "inside".
And the best way to do so is creating another no-name library from scratch!

## Build

```
npm install
npm run build
```

## Alternatives

List of alternative/better/older libs:

 - [pixi-gl-core](https://github.com/pixijs/pixi-gl-core) - I've almost forked it, but found that I need things in a bit different design..
 - [twgl](https://github.com/greggman/twgl.js) - Good kickstart lib for webgl apps
 - [WebGLU](https://github.com/OneGeek/WebGLU) - the older one

## Used sources

 - [webglfundamentals](https://webglfundamentals.org/)
 - [gl-matrix](https://github.com/toji/gl-matrix)

## Feedback

For any questions/propositions/e.t.c you can contact me at <kurzgame@gmail.com>
