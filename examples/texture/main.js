var img = new Image;
img.onload = function(){
    var canvas = document.querySelector("CANVAS");
    var gl = glu.utils.getContext(canvas, {
        "alpha": true,
        "depth": false,
        "stencil": false,
        "antialias": true,
        "premultipliedAlpha": false
    });

    var texture = new glu.Texture(gl, true);
    var testProg = new TestProg(gl);
    
    var texWrap = testProg.texWrap;
    
    // Resize canvas & viewport:
    resize(canvas.offsetWidth, canvas.offsetHeight);
    
    // Prepare texture: ---------------------------------------
    texture.init();
    texture.activate(0);
    texture.bind();
    texture.setData(img);
    texture.setFilteringLinear();
    texture.setClampToEdge();
    
    // Setup texture matrices: --------------------------------
    texWrap.size.resize(texture.widthReal, texture.heightReal);
    
    texWrap.updateToNewSize();

    texWrap.resetTransformMatrix();
    texWrap.setPosition(canvas.width / 2, canvas.height / 2);
    texWrap.setAngle(15);
    texWrap.setScale(1, 1);
    texWrap.setPosition(-texture.width/2, -texture.height/2);

    texWrap.resetSourceFrameMatrix();
    texWrap.setSourceFrame(0, 0, texture.widthReal, texture.heightReal);
    
    texWrap.resetRenderFrameMatrix(canvas.width, canvas.height);
    texWrap.setRenderFrame(0, 0, texture.widthReal, texture.heightReal);
    
    // Prepare & execute program: ----------------------------
    testProg.init();
    testProg.use();
    testProg.update();
    testProg.render();

    // Helpers: ----------------------------------------------

    function resize(width, height){
        canvas.width = width;
        canvas.height = height;
        gl.viewport(0, 0, canvas.width, canvas.height);
    }
};
img.src = "../pic.bmp";

// ---------------------------------------------------------------------------------------
// Program:

function TestProg(gl){
    var VarInfo = glu.VarInfo;

    var colorPosition = VarInfo.makeVarying(VarInfo.T_VEC2, "v_colorPosition");
    
    var vertex = new glu.Vertex(gl, [
        VarInfo.makeAttribute(VarInfo.T_VEC2, "a_position"),
        VarInfo.makeAttribute(VarInfo.T_VEC2, "a_texcoord"),
        VarInfo.makeUniform(VarInfo.T_MAT3, "u_renderMatrix"),
        VarInfo.makeUniform(VarInfo.T_MAT3, "u_textureMatrix"),
        colorPosition
    ], [
        "void main(){",
            "gl_Position = vec4((u_renderMatrix * vec3(a_position, 1)).xy, 0, 1);",
            "v_colorPosition = (u_textureMatrix * vec3(a_texcoord, 1)).xy;",
        "}"
    ].join(""));

    var fragment = new glu.Fragment(gl, [
        colorPosition,
        VarInfo.makeUniform(VarInfo.T_SAMPLER2D, "u_texture0")
    ], [
        "void main(){",
            "gl_FragColor = texture2D(u_texture0, v_colorPosition).bgra;",
        "}"
    ].join(""));

    var texWrap = new glu.TextureMatricesWrapper();

    glu.Program.call(this, gl, vertex, fragment);

    Object.defineProperties(this, {
        "texWrap": {
            "value": texWrap,
            "writable": true,
            "enumerable": true
        },
        "positionBuffer": {
            "value": new glu.ArrayBuffer(gl),
            "writable": true,
            "enumerable": true
        },
        "texcoordBuffer": {
            "value": new glu.ArrayBuffer(gl),
            "writable": true,
            "enumerable": true
        },
        "uniformsPayload": {
            "value": {
                "u_renderMatrix": texWrap.renderFrameMatrix,
                "u_textureMatrix": texWrap.sourceFrameMatrix
            },
            "enumerable": true
        }
    });
}

Object.defineProperties(TestProg.prototype = Object.create(glu.Program.prototype), {
    "constructor": {
        "value": TestProg
    },

    "init": {
        "enumerable": true,
        "value": function(){
            var key;

            this.vertex.init();
            this.fragment.init();
            this.positionBuffer.init();
            this.texcoordBuffer.init();
            glu.Program.prototype.init.call(this);

            for (key in this.uniforms){
                if (!(key in this.uniformsPayload)){
                    // if-statement is for preventing from overwriting already existing values!!
                    // Will not overwrite default values
                    // Very useful after re-init!
                    this.uniformsPayload[key] = this.uniforms[key].info.makeDefaultValue();
                }
            }
        }
    },

    "setupGeometry": {
        "enumerable": true,
        "value": function(){
            var gl = this.gl;
            var verts = new Float32Array([0,0, 0,1, 1,0, 1,0, 0,1, 1,1]);
            var positionLocation = this.attributeLocations.a_position;
            var texcoordLocation = this.attributeLocations.a_texcoord;

            this.positionBuffer.bind();
            this.positionBuffer.setData(verts, gl.STATIC_DRAW);
            gl.enableVertexAttribArray(positionLocation);
            gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 0, 0);

            this.texcoordBuffer.bind();
            this.texcoordBuffer.setData(verts, gl.STATIC_DRAW);
            gl.enableVertexAttribArray(texcoordLocation);
            gl.vertexAttribPointer(texcoordLocation, 2, gl.FLOAT, false, 0, 0);
        }
    },

    "update": {
        "enumerable": true,
        "value": function(){
            var key;
            for (key in this.uniformsPayload){
                this.uniforms[key].setData(this.uniformsPayload[key]);
            }
        }
    },

    "render": {
        "enumerable": true,
        "value": function(){
            glu.utils.renderDefaultVertices(this.gl);
        }
    }
});

