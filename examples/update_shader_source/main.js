document.addEventListener("DOMContentLoaded", function(event) {
    var canvas = document.querySelector("CANVAS");
    var gl = glu.utils.getContext(canvas, {});

    var testProg = new TestProg(gl);

// Resize canvas & viewport:
    canvas.width = canvas.offsetWidth;
    canvas.height = canvas.offsetHeight;
    gl.viewport(0, 0, canvas.width, canvas.height);

// Prepare & execute program: ----------------------------
    testProg.init();
    testProg.use();
    testProg.update();
    testProg.render();

    setTimeout(function() {
        // Wait 1 second and change the whole fragment shader on the fly:

        var fragment = new glu.Fragment(gl, [], "void main(){ gl_FragColor = vec4(1.0, 1.0, 0.0, 1.0); }");
        fragment.init();
        testProg.reattachShader(fragment);

        testProg.update();
        testProg.render();
    }, 1000);

});

// ---------------------------------------------------------------------------------------
// Program:

function TestProg(gl){
    var VarInfo = glu.VarInfo;
    
    var vertex = new glu.Vertex(gl,
        [VarInfo.makeAttribute(VarInfo.T_VEC2, "a_position")],
        "void main(){ gl_Position = vec4(a_position, 0, 1); }");

    var fragment = new glu.Fragment(gl, [], "void main(){ gl_FragColor = vec4(1.0, 0.0, 1.0, 1.0); }");

    glu.Program.call(this, gl, vertex, fragment);

    Object.defineProperties(this, {
        "positionBuffer": {
            "value": new glu.ArrayBuffer(gl),
            "writable": true,
            "enumerable": true
        }
    });
}

Object.defineProperties(TestProg.prototype = Object.create(glu.Program.prototype), {
    "constructor": {
        "value": TestProg
    },

    "init": {
        "enumerable": true,
        "value": function(){
            this.vertex.init();
            this.fragment.init();
            this.positionBuffer.init();

            glu.Program.prototype.init.call(this);
        }
    },

    "setupGeometry": {
        "enumerable": true,
        "value": function(){
            var gl = this.gl;
            var verts = new Float32Array([
                -1.0, -1.0,
                1.0, -1.0,
                -1.0,  1.0,
                -1.0,  1.0,
                1.0, -1.0,
                1.0,  1.0]);
            var positionLocation = this.attributeLocations.a_position;

            this.positionBuffer.bind();
            this.positionBuffer.setData(verts, gl.STATIC_DRAW);
            gl.enableVertexAttribArray(positionLocation);
            gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 0, 0);
        }
    },

    "update": {
        "enumerable": true,
        "value": function(){
            // Do nothing
        }
    },

    "render": {
        "enumerable": true,
        "value": function(){
            glu.utils.renderDefaultVertices(this.gl);
        }
    }
});

